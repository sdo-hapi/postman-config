# Postman Collections & Environments for the Harmonized APIs

This GIT repository contains a predefined Collections and Environments ready to import into the
[Postman](https://www.postman.com/product/tools/) API testing platform. You wish to use it for a quick
startup to start practicing the Harmonized APIs.

The first step is to download and install [Postman](https://www.postman.com/downloads/).

Note before importing: 
- Postman collections and environments have been exported in Postman Collection V2.1 format.
- You need to obtain valid credentials. Please follow instructions [for ISO](https://api-portal.iso.org),  [for CEN](https://api-portal.cen.eu/) and [for CENELEC](https://api-portal.cenelec.eu).

___

## How to use Postman with Harmonized APIs?

The first step is to create a **personal workspace**.
### Import an environment
Only import environments that you are willing to test.

Environment files follow the following pattern: *envType*_**tenant**.postman_environment.json where:
  - *envType* is for "test" or "prod" (for production) environment 
  - **tenant** is either "iso", "cen" or "cenelec" API Portals. 

#### You need to set your App key and your App secret in all the environments you are going to use 


![Set Up credentials in an environment](doc/SetUpCredentialsInEnvironment.png)

For security reason, please only fill current value field not the initial value field.
**Do not forget to save your environment.**

___

### Import Harmonized Projects and Publications.postman_collection.json file

#### Edit the collection and go on the Authorisation tab

![Edit the collection to set access token](doc/EditTheProjectsAndPublicationsCollection.png)

#### Set the access token with the variable ***{{OAuth_Token}}***

The collection is equipped with JavaScript that will automate the retrieval of an access token, this is to simplify
your life. The only thing you have to specify is where all requests from the collection will find the token.

_Note: If you have not yet defined on which environment your collection will operate, the {{OAuth_Token}} 
will remain yellow as in the screenshot below. This is not a problem.
If you had selected it the variable will be recognized._


![Set an access token](doc/SetAccessToken.png)

Select an environemnt from the upper right menu (see yellow circle).
You are ready to test the APIs. The authentication will be automatically performed for you
and will be redone when the token will expired. The Oauth2 authentication is performed in a
pre-scripts at the collection level.

![Choose an environment](doc/SelectAnEnvironment.png)



___

### Check the link header to verify if pagination is required to get all elements


![Check link header](doc/CheckPaginationInHeader.png)


***If your app is granted for Harmonized Working Committee documents***, then you can also import the collection Harmonized Committee and Working Documents.postman_collection.json file. You will need also to set the OAuth_Token variable.
If you are getting a status code error 401 in the Committee Working Documents in the test environment, there is something missing in you profile. Please contact me (sevel@iso.org) or the helpdesk (helpdesk@iso.org).

---
### Notes

* The ISO Harmonized API for Projects&Publications test environment is very close to the production environment. But it is not the case for the other APIs.
* Do not forget that for a request, you will get only at most the first "size" (ie: 10, 100, 500, etc...) elements. 
To get all the results, you will have to extract the number of pages from the ["Link" response header](https://connect.iso.org/pages/viewpage.action?pageId=216957829). 


---
### TIPS

For all the search requests, the number of elements in the page is displayed in the console. So to know the total numbers of elements corresponding to the request:

  * Perform a first request with some request parameters with page=0 and size=100
  * Check the header response link to get the last page number "y"
  * Perform another request with the same request parameters with page=y and size=100
  * Then the total numbers of elements is y*100 + number of elements in the last request.
